using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
	private GameObject target;
	[SerializeField]
	private string targetTag = "Player";

	// amount of damage that will be dealt by subject
	public float damageAmount = 10.0f;

	// is set to true, if subject will deal damage on trigger
	public bool damageOnTrigger = false;

	// is set to true, if subject will deal damage on collision
	public bool damageOnCollision = false;

	// is set to true, if subject will deal continuous damage
	public bool continuousDamage = false;
	// time between damage dealing
	public float continuousTimeBetweenHits = 0;
	// is used to check if time between hits has passed
	private float savedTime = 0;

	// is set to true, if subject will destroy self on impact
	public bool destroySelfOnImpact = false;
	public float delayBeforeDestroy = 0.0f;
	// prefab that will be used in the effect after self destruction
	public GameObject explosionPrefab;

	// Start is called before the first frame update
	void Start()
	{
		target = GameObject.FindWithTag(targetTag);
	}

	// Update is called once per frame
	void Update()
    {
        if (Vector3.Distance(this.transform.position, target.transform.position) <= 1)
        {
			// if the hit object has the Health script on it, deal damage
			if (target.GetComponent<Health>() != null)
			{
				target.GetComponent<Health>().ApplyDamage(damageAmount);

				if (destroySelfOnImpact)
				{
					// destroy the object whenever it hits something
					Destroy(gameObject, delayBeforeDestroy);
				}

				if (explosionPrefab != null)
				{
					Instantiate(explosionPrefab, transform.position, transform.rotation);
				}
			}
		}
    }

    // used for things like bullets, which are triggers.
    void OnTriggerEnter(Collider collision)                      
	{
		if (damageOnTrigger)
		{
			// if the player got hit with it's own bullets, ignore it
			if (this.tag == "PlayerBullet" && collision.gameObject.tag == "Player")
				return;

			// if the hit object has the Health script on it, deal damage
			if (collision.gameObject.GetComponent<Health>() != null)
			{
				collision.gameObject.GetComponent<Health>().ApplyDamage(damageAmount);

				if (destroySelfOnImpact)
				{
					// destroy the object whenever it hits something
					Destroy(gameObject, delayBeforeDestroy);
				}

				if (explosionPrefab != null)
				{
					Instantiate(explosionPrefab, transform.position, transform.rotation);
				}
			}
		}
	}

	// this is used for things that explode on impact and are NOT triggers
	void OnCollisionEnter(Collision collision)
	{
		if (damageOnCollision)
		{
			// if the player got hit with it's own bullets, ignore it
			if (this.tag == "PlayerBullet" && collision.gameObject.tag == "Player")
				return;

			// if the hit object has the Health script on it, deal damage
			if (collision.gameObject.GetComponent<Health>() != null)
			{
				collision.gameObject.GetComponent<Health>().ApplyDamage(damageAmount);

				if (destroySelfOnImpact)
				{
					// destroy the object whenever it hits something
					Destroy(gameObject, delayBeforeDestroy);
				}

				if (explosionPrefab != null)
				{
					Instantiate(explosionPrefab, transform.position, transform.rotation);
				}
			}
		}
	}

	// this is used for damage over time things
	void OnCollisionStay(Collision collision)
	{
		if (continuousDamage)
		{
			// is only triggered if whatever it hits is the player
			if (collision.gameObject.tag == "Player" && collision.gameObject.GetComponent<Health>() != null)
			{
				// checks if time has passed and damage can be dealt
				if (Time.time - savedTime >= continuousTimeBetweenHits)
				{
					savedTime = Time.time;
					collision.gameObject.GetComponent<Health>().ApplyDamage(damageAmount);
				}
			}
		}
	}
}
