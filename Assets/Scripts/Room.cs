// class that defines room in the maze which contains coordinates of its upper left and lower right angles
public class Room
{
    // x coordinate of upper left angel of the room
    public int x1;
    // x coordinate of lower right angel of the room
    public int x2;
    // y coordinate of upper left angel of the room
    public int z1;
    // y coordinate of lower right angel of the room
    public int z2;
    // original room is divided into two rooms which are separated by wall
    public Room left;
    public Room right;

    // constructor for object of this class
    public Room(int x1, int x2, int z1, int z2)
    {
        this.x1 = x1;
        this.x2 = x2;
        this.z1 = z1;
        this.z2 = z2;
    }
}