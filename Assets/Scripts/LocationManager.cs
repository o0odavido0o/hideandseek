using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour
{
    // number of columns in our game location.
    [SerializeField]
    private int columns = 8;
    // number of rows in our game location.
    [SerializeField]
    private int rows = 8;
    [SerializeField]
    private int recursionDepth = 4;
    // min room width
    //[SerializeField]
    //private int xMinRoom = 3;
    // min room length
    //[SerializeField]
    //private int yMinRoom = 3;
    // prefab to spawn for floor
    [SerializeField]
    private GameObject cell;
    // prefab to spawn for maze
    [SerializeField]
    private GameObject wall;
    [SerializeField]
    private GameObject referencePoint;
    // a variable to store a reference to the transform of our Location object.
    private Transform locationHolder;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Sets up the maze and floor of the level.
    void LocationSetup()
    {
        // Instantiate location and set locationHolder to its transform.
        locationHolder = new GameObject("Location").transform;
        locationHolder.gameObject.tag = "Location";

        // Loop along x axis, starting from -1 (to fill corner) with cell prefabs.
        for (int x = 0; x < columns + 2; x++)
        {
            // Loop along y axis, starting from -1 to place cell prefabs.
            for (int z = 0; z < rows + 2; z++)
            {
                // koefficient that transform scale to size
                float koefTransScaleSize = 10;

                float cellLength = cell.transform.localScale.x * koefTransScaleSize;
                float cellWidth = cell.transform.localScale.z * koefTransScaleSize;

                // Instantiate the cell at the Vector3 corresponding to current grid position in loop, cast it to GameObject.
                GameObject instance =
                    Instantiate(cell, new Vector3(cellLength * x, 0f, cellWidth * z), Quaternion.identity) as GameObject;

                // Set the parent of our newly instantiated object instance to locationHolder, this is just organizational to avoid cluttering hierarchy.
                instance.transform.SetParent(locationHolder);

                // Sets up the maze
                koefTransScaleSize = 1;
                float blockLength = wall.transform.localScale.x * koefTransScaleSize;
                float blockWidth = wall.transform.localScale.z * koefTransScaleSize;
                if ((x == 0 || x == columns + 1 || z == 0 || z == rows + 1))
                {
                    instance = Instantiate(wall, new Vector3(blockLength * x, 1.5f, blockWidth * z), Quaternion.identity) as GameObject;
                    // Set the parent of our newly instantiated object instance to locationHolder, this is just organizational to avoid cluttering hierarchy.
                    instance.transform.SetParent(locationHolder);
                }
            }
        }

        // creates the maze
        Maze maze = new Maze();
        maze.CreateMaze(1, columns, 1, rows, recursionDepth, this);
    }

    // SetupScene initializes our level and calls the previous functions to lay out the game location
    public void SetupScene()
    {
        // creates the maze and floor
        LocationSetup();
    }

    // function that instanciate wall prefab with (x, 1.5f, z) coordinate
    public GameObject InstantiateWall(int x, int z)
    {
        GameObject instance = Instantiate(wall, new Vector3(x, 1.5f, z), Quaternion.identity) as GameObject;
        // Set the parent of our newly instantiated object instance to locationHolder, this is just organizational to avoid cluttering hierarchy.
        instance.transform.SetParent(locationHolder);

        return instance;
    }

    // function that instanciate wall prefab with (x, 1.5f, z) coordinate
    public GameObject InstantiateReferencePoint(int x, int z)
    {
        GameObject instance = Instantiate(referencePoint, new Vector3(x, 1.5f, z), Quaternion.identity) as GameObject;
        // Set the parent of our newly instantiated object instance to locationHolder, this is just organizational to avoid cluttering hierarchy.
        instance.transform.SetParent(locationHolder);

        return instance;
    }
}
