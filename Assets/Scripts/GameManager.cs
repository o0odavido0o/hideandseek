using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;

    private enum gameStates { Playing, BeatLevel, GameOver};
    private gameStates gameState = gameStates.Playing;

    [SerializeField]
    private float startTime = 5.0f;
    private float currentTime;

    [SerializeField]
    private GameObject player;
    private Health playerHealth;

    [SerializeField]
    private GameObject enemy;
    [SerializeField]
    private NavMeshSurface surface;
    // store a reference to our LocationManager which will set up the location.
    private LocationManager locationScript;

    [SerializeField]
    private Text timerDisplay;

    [SerializeField]
    private GameObject beatLevelButtons;

    [SerializeField]
    private GameObject pauseMenuButtons;

    [SerializeField]
    private GameObject gameOverButtons;

    // awake is always called before any Start functions
    void Awake()
    {
        // check if instance already exists
        if (instance == null)
        {
            // if not, set instance to this
            instance = this;
        }
        // if instance already exists and it's not this:
        else if (instance != this)
        {
            // then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }

        // get a component reference to the attached LocationManager script
        locationScript = GetComponent<LocationManager>();

        // call the InitGame function to initialize the game
        InitGame();

        playerHealth = player.GetComponent<Health>();

        // set the current time to the startTime specified
        currentTime = startTime;
    }

    // Update is called once per frame
    void LateUpdate()
    {

        switch (gameState)
        {
            case gameStates.Playing:

                currentTime -= Time.deltaTime;
                if (currentTime >= 0)
                {
                    timerDisplay.text = "Time left: " + currentTime.ToString("0.00");
                }
                else
                {
                    timerDisplay.text = "Time left: " + 0f.ToString("0.00");
                }

                if (playerHealth.isAlive == false)
                {
                    // pause game
                    Time.timeScale = 0;
                    gameOverButtons.SetActive(true);
                    gameState = gameStates.GameOver;
                }
                else if (currentTime <= 0)
                {
                    // pause game
                    Time.timeScale = 0;
                    beatLevelButtons.SetActive(true);
                    gameState = gameStates.BeatLevel;
                }
                else
                {
                    // if ESCAPE key is pressed
                    if (Input.GetButtonDown("Cancel"))
                    {
                        // stops/unstops time based on previous state of time
                        Time.timeScale = Time.timeScale > 0 ? 0 : 1;
                        // shows or removes pause menu based on state of time
                        pauseMenuButtons.SetActive(Time.timeScale == 0);
                    }
                }
                break;
            case gameStates.BeatLevel:
                // nothing
                break;
            case gameStates.GameOver:
                // nothing
                break;
        }
    }

    // initializes the game
    void InitGame()
    {
        // call the SetupScene function of the LevelManager script
        locationScript.SetupScene();

        // build navigation mesh which will be used by AI to walk through maze
        surface.BuildNavMesh();

        // spawn player
        player = Instantiate(player, new Vector3(3f, 1f, 3f), Quaternion.identity) as GameObject;

        // spawn enemy
        GameObject enemyInstance = Instantiate(enemy, new Vector3(30f, 1f, 30f), Quaternion.identity) as GameObject;
    }

    // function for button 'continue' in the pause menu
    public void Continue()
    {
        pauseMenuButtons.SetActive(false);
        Time.timeScale = 1;
    }

    // loads main scene with gameplay
    public void LoadLevel()
    {
        Application.LoadLevel("Level");
        Time.timeScale = 1;
    }

    public void LoadMainMenu()
    {
        Application.LoadLevel("MainMenu");
        Time.timeScale = 1;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}