using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
	public enum deathAction { loadLevelWhenDead, doNothingWhenDead };
	public deathAction onLivesGone = deathAction.doNothingWhenDead;

	public float healthPoints = 1f;
	// base health points
	public float respawnHealthPoints = 1f;

	// lives and variables for respawning
	public int numberOfLives = 1;
	public bool isAlive = true;

	// prefab that will be used in the effect after self destruction
	public GameObject explosionPrefab;

	public string LevelToLoad = "";

	private Vector3 respawnPosition;
	private Quaternion respawnRotation;



	// Use this for initialization
	void Start()
	{
		// store initial position as respawn location
		respawnPosition = transform.position;
		respawnRotation = transform.rotation;

		// default to current scene 
		if (LevelToLoad == "")
		{
			LevelToLoad = Application.loadedLevelName;
		}
	}

	// Update is called once per frame
	void Update()
	{
		// if the object is 'dead'
		if (healthPoints <= 0)
		{	// decrement # of lives
			numberOfLives--;

			if (explosionPrefab != null)
			{
				Instantiate(explosionPrefab, transform.position, Quaternion.identity);
			}

			// respawn
			if (numberOfLives > 0)
			{   // reset the player to respawn position
				transform.position = respawnPosition;   
				transform.rotation = respawnRotation;
				// give the player full health again
				healthPoints = respawnHealthPoints;
			}
			else
			{	// here is where you do stuff once ALL lives are gone)
				isAlive = false;

				switch (onLivesGone)
				{
					case deathAction.loadLevelWhenDead:
						Application.LoadLevel(LevelToLoad);
						break;
					case deathAction.doNothingWhenDead:
						// do nothing, death must be handled in another way elsewhere
						break;
				}
			}
		}
	}

	public void ApplyDamage(float amount)
	{
		healthPoints = healthPoints - amount;
	}

	public void ApplyHeal(float amount)
	{
		healthPoints = healthPoints + amount;
	}

	public void ApplyBonusLife(int amount)
	{
		numberOfLives = numberOfLives + amount;
	}

	public void updateRespawn(Vector3 newRespawnPosition, Quaternion newRespawnRotation)
	{
		respawnPosition = newRespawnPosition;
		respawnRotation = newRespawnRotation;
	}
}
