using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField]
    private float distEps = 1f;
    [SerializeField]
    private float chaseTimeOut = 5f;
    [SerializeField]
    private float fieldOfView = 60f;
    [SerializeField]
    private string sightMask = "Maze";


    // diffrent states of enemy
    private enum ENEMY_STATE { IDLE = 2081823275,
                               PATROL = 207038023,
                               CHASE = 1463555229 };
    // current state of enemy
    private ENEMY_STATE currentState = ENEMY_STATE.IDLE;

    private Animator enemyAnimator;
    private UnityEngine.AI.NavMeshAgent enemyAgent;
    private Transform enemyTransform;
    private BoxCollider enemyBoxCollider;

    private Transform playerTransform;

    private Transform[] referencePointsTransform;

    private bool canSeePlayer = true;



    void Awake()
    {
        enemyAnimator = GetComponent<Animator>();
        enemyAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        enemyTransform = transform;
        enemyBoxCollider = GetComponent<BoxCollider>();

        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        GameObject[] ReferencePoints = GameObject.FindGameObjectsWithTag("ReferencePoint");
        referencePointsTransform = (from GameObject GO in ReferencePoints select GO.transform).ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        canSeePlayer = false;

        if (!enemyBoxCollider.bounds.Contains(playerTransform.position)) return;

        // checks if enemy can see player
        canSeePlayer = HaveLineSightToPlayer(playerTransform);
    }

    private IEnumerator State_Idle()
    {
        currentState = ENEMY_STATE.IDLE;

        // activates idle state
        enemyAnimator.SetTrigger((int)ENEMY_STATE.IDLE);

        // stops enemy movement
        enemyAgent.Stop();

        while(currentState == ENEMY_STATE.IDLE)
        {
            if (canSeePlayer)
            {
                // makes enemy start chasing player
                StartCoroutine(State_Chase());
                yield break;
            }

            // waits for next frame
            yield return null;
        }
    }

    private IEnumerator State_Patrol()
    {
        currentState = ENEMY_STATE.PATROL;

        // activates idle state
        enemyAnimator.SetTrigger((int)ENEMY_STATE.PATROL);

        // chooses random destination
        Transform randomDestination = referencePointsTransform[Random.Range(0, referencePointsTransform.Length)];

        enemyAgent.SetDestination(randomDestination.position);

        // resumes enemy movement cause it was stopped in State_Idle
        enemyAgent.Resume();

        while (currentState == ENEMY_STATE.PATROL)
        {
            if (canSeePlayer)
            {
                // makes enemy start chasing player
                StartCoroutine(State_Chase());
                yield break;
            }

            bool enemyGotToRandomDestination = Vector3.Distance(enemyTransform.position, randomDestination.position) <= distEps;
            if (enemyGotToRandomDestination)
            {
                // makes enemy start idlying
                StartCoroutine(State_Idle());
                yield break;
            }

            // waits for next frame
            yield return null;
        }
    }

    private IEnumerator State_Chase()
    {
        currentState = ENEMY_STATE.CHASE;

        // activates chase state
        enemyAnimator.SetTrigger((int)ENEMY_STATE.CHASE);

        while (currentState == ENEMY_STATE.CHASE)
        {
            // chase the player
            enemyAgent.SetDestination(playerTransform.position);

            bool dontSeePlayer = !canSeePlayer;
            if (dontSeePlayer)
            {
                // time that has passed since the start of chase
                float elapsedTime = 0f;

                while (true)
                {
                    elapsedTime += Time.deltaTime;

                    enemyAgent.SetDestination(playerTransform.position);

                    yield return null;

                    if (elapsedTime >= chaseTimeOut)
                    {
                        dontSeePlayer = !canSeePlayer;
                        if (dontSeePlayer)
                        {
                            // makes enemy start idlying
                            StartCoroutine(State_Idle());
                            yield break;
                        }
                        // enemyAI again sees player so elapsedTime is restarted 
                        else
                            break;
                    }
                }
            }

            // waits for next frame
            yield return null;
        }
    }

    public void OnIdleAnimCompleted()
    {
        StopAllCoroutines();
        StartCoroutine(State_Patrol());
    }

    // checks if enemy can see player
    public bool HaveLineSightToPlayer(Transform player)
    {
        float angle = Mathf.Abs(Vector3.Angle(enemyTransform.forward, (player.position - enemyTransform.position).normalized));

        if (angle > fieldOfView) return false;

        if (Physics.Linecast(enemyTransform.position, player.position, LayerMask.GetMask(sightMask)))
            return false;

        return true;
    }
}