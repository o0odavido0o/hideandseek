using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze
{
    // contains whole maze
    private Room maze;
    // x coordinate of upper left angel of the room
    private int x1Maze;
    // x coordinate of lower right angel of the room
    private int x2Maze;
    // z coordinate of upper left angel of the room
    private int z1Maze;
    // z coordinate of lower right angel of the room
    private int z2Maze;
    // min room width
    private int xMinRoom = 3;
    // min room length
    private int yMinRoom = 3;
    private LocationManager LM;
    // keeps walls of maze
    // by getting its values like walls[z, x], you get wall with coordinate (x, 1.5f, z)
    private List<List<GameObject>> walls;


    // calls function that will create maze
    public void CreateMaze(int x1, int x2, int z1, int z2, int recursionDepth, LocationManager LM)
    {
        x1Maze = x1;
        x2Maze = x2;
        z1Maze = z1;
        z2Maze = z2;
        this.LM = LM;
        // fill matrix (list of lists) with empty objects so i can easily save walls
        walls = new List<List<GameObject>>();
        for (int i = 0; i <= x2 + 2; i++)
        {
            walls.Add(new List<GameObject>());
            for (int j = 0; j <= z2 + 2; j++)
            {
                walls[i].Add(null);
            }
        }
        // creates maze for level
        DoCreateMaze(maze, x1, x2, z1, z2, recursionDepth);
    }

    // creates maze for level
    public int DoCreateMaze(Room room, int x1, int x2, int z1, int z2, int recursionDepth)
    {
        // creates new room
        room = new Room(x1, x2, z1, z2);

        // in which direction (x ~ 0 || y ~ 1) wall will be created
        int direction = 0;
        // coordinate of wall
        int wallCoord;

        // makes sure that too small rooms are not created
        if (recursionDepth <= 0)
        {
            int random;

            // destroys one block in right wall of the room to create exit in it
            // destroys if it's not a wall surrounding a maze 
            if (x2 != x2Maze)
            {
                do
                    random = Random.Range(z1, z2);
                while (walls[random][x2] != null || walls[random][x2 + 2] != null);
                GameObject.DestroyImmediate(walls[random][x2 + 1]);
                walls[random][x2 + 1] = null;
            }

            // destroys one block in left wall of the room to create exit in it
            // destroys if it's not a wall surrounding a maze 
            if (x1 != x1Maze)
            {
                do
                    random = Random.Range(z1, z2);
                while (walls[random][x1 - 2] != null || walls[random][x1] != null);
                GameObject.DestroyImmediate(walls[random][x1 - 1]);
                walls[random][x1 - 1] = null;
            }

            // destroys one block in upper wall of the room to create exit in it
            // destroys if it's not a wall surrounding a maze 
            if (z2 != z2Maze)
            {
                do
                    random = Random.Range(x1, x2);
                while (walls[z2][random] != null || walls[z2 + 2][random] != null);
                GameObject.DestroyImmediate(walls[z2 + 1][random]);
                walls[z2 + 1][random] = null;
            }

            // destroys one block in lower wall of the room to create exit in it
            // destroys if it's not a wall surrounding a maze 
            if (z1 != z1Maze)
            {
                do
                    random = Random.Range(x1, x2);
                while (walls[z1 - 2][random] != null || walls[z1][random] != null);
                GameObject.DestroyImmediate(walls[z1 - 1][random]);
                walls[z1 - 1][random] = null;
            }

            // instantiate reference point that will help AI explore maze
            LM.InstantiateReferencePoint(x1 + (x2 - x1) / 2, z1 + (z2 - z1) / 2);

            return -1;
        }

        // chooses direction of wall spawn (at the longest wall)
        direction = x2 - x1 >= z2 - z1 ? 0 : 1;
        // spawns walls and creates new rooms
        if (direction == 0)
        {
            // chooses at which point walls will be spawned
            if (x2 - x1 + 1 >= 2 * (z2 - z1 + 1) + 1)
            {
                wallCoord = Random.Range(x1 + (z2 - z1 + 1), x2 - (z2 - z1 + 1) + 1);
            }
            else
            {
                wallCoord = Random.Range(x1 + (z2 - z1 + 1) / 3 + 1, x2 - (z2 - z1 + 1) / 3);
            }

            // spawns walls
            for (int i = z1; i <= z2; i++)
            {
                walls[i][wallCoord] = LM.InstantiateWall(wallCoord, i);
            }

            // creates new rooms
            recursionDepth--;
            DoCreateMaze(room.right, wallCoord + 1, x2, z1, z2, recursionDepth);
            DoCreateMaze(room.left, x1, wallCoord - 1, z1, z2, recursionDepth);
        }
        else
        {
            // chooses at which point walls will be spawned
            if (z2 - z1 + 1 >= 2 * (x2 - x1 + 1) + 1)
            {
                wallCoord = Random.Range(z1 + (x2 - x1 + 1), z2 - (x2 - x1 + 1) + 1);
            }
            else
            {
                wallCoord = Random.Range(z1 + (x2 - x1 + 1) / 3 + 1, z2 - (x2 - x1 + 1) / 3);
            }

            // spawns walls
            for (int i = x1; i <= x2; i++)
            {
                walls[wallCoord][i] = LM.InstantiateWall(i, wallCoord);
            }

            // creates new rooms
            recursionDepth--;
            DoCreateMaze(room.right, x1, x2, wallCoord + 1, z2, recursionDepth);
            DoCreateMaze(room.left, x1, x2, z1, wallCoord - 1, recursionDepth);
        }

        return wallCoord;
    }
}